import * as categoryActionTypes from '../actionTypes/categoryActionTypes';
import { getCategoryApi, getCategoryByIdApi } from '../../api/categoryApi';
import { getLang } from './receiptAction';

export const getCategoryList = () => (dispatch: any) => {
  return dispatch({
    api: getCategoryApi,
    types: [
      categoryActionTypes.CATEGORY_REQUEST,
      categoryActionTypes.CATEGORY_SUCCESS,
      categoryActionTypes.CATEGORY_ERROR,
    ],
    query: { lang: getLang() },
  });
};

export const getCategoryById = (id) => (dispatch: any) => {
  return dispatch({
    api: getCategoryByIdApi,
    types: [
      categoryActionTypes.CATEGORY_ID_REQUEST,
      categoryActionTypes.CATEGORY_ID_SUCCESS,
      categoryActionTypes.CATEGORY_ID_ERROR,
    ],
    query: { id, lang: getLang() },
  });
};
