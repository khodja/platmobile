import * as sliderActionTypes from "../actionTypes/sliderActionTypes";
import { sliderApi } from "../../api/sliderApi";

export const getSlider = () => (dispatch: any) => {
  return dispatch({
    api: sliderApi,
    types: [
      sliderActionTypes.SLIDER_REQUEST,
      sliderActionTypes.SLIDER_SUCCESS,
      sliderActionTypes.SLIDER_ERROR,
    ],
    query: {},
  });
};