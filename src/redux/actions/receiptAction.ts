import * as receiptActionTypes from '../actionTypes/receiptActionTypes';
import { getReceiptApi, getReceiptByIdApi } from '../../api/receiptApi';
import { store } from '../storeConfig';

export const getLang = () => {
  return store.getState().lang;
};
export const getReceiptList = () => (dispatch: any) => {
  return dispatch({
    api: getReceiptApi,
    types: [
      receiptActionTypes.RECEIPT_REQUEST,
      receiptActionTypes.RECEIPT_SUCCESS,
      receiptActionTypes.RECEIPT_ERROR,
    ],
    query: { lang: getLang() },
  });
};

export const getReceiptById = (id) => (dispatch: any) => {
  return dispatch({
    api: getReceiptByIdApi,
    types: [
      receiptActionTypes.RECEIPT_REQUEST,
      receiptActionTypes.RECEIPT_ID_SUCCESS,
      receiptActionTypes.RECEIPT_ID_ERROR,
    ],
    query: { id },
  });
};
