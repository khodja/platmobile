import * as TYPES from '../actionTypes/searchActionTypes';
import { searchApi } from '../../api/searchApi';
import { getLang } from './receiptAction';

export const search = (params = null) => (dispatch: any) => {
  const isMore = params?.page > 1 ? '_MORE' : '';
  return dispatch({
    api: searchApi,
    types: [
      TYPES[`SEARCH${isMore}_REQUEST`],
      TYPES[`SEARCH${isMore}_SUCCESS`],
      TYPES.SEARCH_ERROR,
    ],
    query: { ...params, lang: getLang() },
  });
};
