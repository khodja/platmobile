import { LANG } from '../actionTypes/langActionTypes';

export const changeLang = (lang) => {
  return {
    type: LANG,
    payload: lang,
  };
};
