import * as TYPES from '../actionTypes/filterActionTypes';

export const switchFilter = () => {
  return {
    type: TYPES.SWITCH_FILTER,
  };
};
export const resetFilter = () => {
  return {
    type: TYPES.RESET_FILTER,
  };
};
export const chooseFilter = (payload = { key: '', value: '' }) => {
  return {
    type: TYPES.CHOICE_FILTER,
    payload,
  };
};

export const openFilter = (payload = '') => {
  return {
    type: TYPES.OPEN_FILTER,
    payload,
  };
};
