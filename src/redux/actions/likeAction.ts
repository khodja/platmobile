import * as likeActionTypes from '../actionTypes/likeActionTypes';

export const addLike = (item?) => {
  if (!!item) {
    return {
      data: item,
      type: likeActionTypes.LIKE_ADD,
    };
  }
};
export const removeLike = (id?: number) => {
  if (id) {
    return {
      id,
      type: likeActionTypes.LIKE_REMOVE,
    };
  }
};
export const setReview = (isPassed = true) => {
  return {
    data: Boolean(isPassed),
    type: likeActionTypes.REVIEW_PASSED,
  };
};
export const incrementView = () => {
  return {
    type: likeActionTypes.INCREMENT_VIEW,
  };
};
