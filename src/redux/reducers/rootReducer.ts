import { combineReducers } from 'redux';
import search from './searchReducer';
import receipt from './receiptReducer';
import category from './categoryReducer';
import slider from './sliderReducer';
import liked from './likedReducer';
import lang from './langReducer';
import filter from './filterReducer';

export default combineReducers({
  search,
  receipt,
  category,
  slider,
  liked,
  lang,
  filter,
});
