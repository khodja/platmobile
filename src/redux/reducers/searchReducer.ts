import * as TYPES from '../actionTypes/searchActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: false,
  error: null,
  receipts: [],
  hasMore: false,
  moreLoading: false,
};

const reducers = {
  [TYPES.SEARCH_REQUEST](state = initState) {
    state.receipts = [];
    state.loading = true;
    state.moreLoading = false;
    state.hasMore = false;
    state.error = null;
  },
  [TYPES.SEARCH_SUCCESS](
    state = initState,
    action: { payload: { data: any[]; next_page_url: any } },
  ) {
    state.receipts = action.payload.data;
    state.hasMore = !!action.payload.next_page_url;
    state.error = null;
    state.loading = false;
  },
  [TYPES.SEARCH_ERROR](state = initState, action: { error: null }) {
    state.loading = false;
    state.moreLoading = false;
    state.error = action.error;
    state.hasMore = false;
    state.receipts = [];
  },
  [TYPES.SEARCH_MORE_REQUEST](state = initState) {
    state.moreLoading = true;
    state.hasMore = false;
    state.error = null;
  },
  [TYPES.SEARCH_MORE_SUCCESS](
    state: any,
    action: { payload: { data: any; next_page_url: any } },
  ) {
    state.receipts = [...state.receipts, ...action.payload.data];
    state.error = null;
    state.hasMore = !!action.payload.next_page_url;
    state.moreLoading = false;
  },
};

export default createReducer(initState, reducers);
