import {LANG} from '../actionTypes/langActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  lang: 1,
};

const reducers = {
  [LANG](state, action) {
    state.lang = action.payload;
  },
};

export default createReducer(initState, reducers);
