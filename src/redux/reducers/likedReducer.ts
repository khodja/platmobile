import * as likeActionTypes from '../actionTypes/likeActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  data: [],
  reviewPassed: false,
  viewedCount: 0,
};

const reducers = {
  [likeActionTypes.LIKE_ADD](state, action) {
    state.data = [...state.data, action.data];
  },
  [likeActionTypes.LIKE_REMOVE](state, action) {
    state.data = state.data.filter((item: any) => item.id != action.id);
  },
  [likeActionTypes.REVIEW_PASSED](state, action) {
    state.reviewPassed = action.data;
  },
  [likeActionTypes.INCREMENT_VIEW](state) {
    state.viewedCount = state.viewedCount + 1;
  },
};

export default createReducer(initState, reducers);
