import * as sliderActionTypes from '../actionTypes/sliderActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  loading: false,
  data: [],
  error: null,
};

const reducers = {
  [sliderActionTypes.SLIDER_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [sliderActionTypes.SLIDER_SUCCESS](state, action) {
    state.loading = false;
    state.data = action.payload.data;
  },
  [sliderActionTypes.SLIDER_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.data = null;
  },
};

export default createReducer(initState, reducers);
