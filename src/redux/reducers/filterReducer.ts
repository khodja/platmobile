import * as TYPES from '../actionTypes/filterActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  filter: true,
  calory: null,
  caloryOpen: false,
  duration: null,
  durationOpen: false,
  category: null,
  categoryOpen: false,
};

const reducers = {
  [TYPES.SWITCH_FILTER](state = initState) {
    if (state.filter) {
      if (state.caloryOpen) state.caloryOpen = false;
      if (state.durationOpen) state.durationOpen = false;
      if (state.categoryOpen) state.categoryOpen = false;
    }
    state.filter = !state.filter;
  },
  [TYPES.CHOICE_FILTER](state = initState, action) {
    state[action.payload.key] = action.payload.value;
    state[action.payload.key + 'Open'] = false;
  },
  [TYPES.OPEN_FILTER](state = initState, action) {
    state[action.payload + 'Open'] = true;
  },
  [TYPES.RESET_FILTER](state) {
    state.filter = true;
    state.calory = null;
    state.caloryOpen = false;
    state.duration = null;
    state.durationOpen = false;
    state.category = null;
    state.categoryOpen = false;
  },
};

export default createReducer(initState, reducers);
