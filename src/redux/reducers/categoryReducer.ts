import * as categoryActionTypes from "../actionTypes/categoryActionTypes";
import { createReducer } from "../../utils/storeUtils";

const initState = {
  loading: false,
  categories: [],
  category: [],
  error: null,
};

const reducers = {
  [categoryActionTypes.CATEGORY_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [categoryActionTypes.CATEGORY_SUCCESS](state, action) {
    state.loading = false;
    state.categories = action.payload.data;
  },
  [categoryActionTypes.CATEGORY_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.categories = [];
  },
  [categoryActionTypes.CATEGORY_ID_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [categoryActionTypes.CATEGORY_ID_SUCCESS](state, action) {
    state.loading = false;
    state.category = action.payload.data;
  },
  [categoryActionTypes.CATEGORY_ID_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.category = [];
  },
};

export default createReducer(initState, reducers);
