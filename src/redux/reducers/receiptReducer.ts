import * as receiptActionTypes from '../actionTypes/receiptActionTypes';
import { createReducer } from '../../utils/storeUtils';

const initState = {
  receipts: [],
  receipt: null,
  loading: true,
  receiptLoading: true,
  error: null,
};

const reducers = {
  [receiptActionTypes.RECEIPT_REQUEST](state) {
    state.loading = true;
    state.error = null;
  },
  [receiptActionTypes.RECEIPT_SUCCESS](state, action) {
    state.receipts = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [receiptActionTypes.RECEIPT_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.receipts = [];
  },
  [receiptActionTypes.RECEIPT_ID_SUCCESS](state, action) {
    state.receipt = action.payload.data;
    state.loading = false;
    state.error = null;
  },
  [receiptActionTypes.RECEIPT_ID_ERROR](state, action) {
    state.loading = false;
    state.error = action.error;
    state.receipt = null;
  },
};

export default createReducer(initState, reducers);
