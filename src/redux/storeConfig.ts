import { createStore, applyMiddleware } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import rootReducer from './reducers/rootReducer';
import { persistStore, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import apiMiddleware from './middlewares/apiMiddleware';

const persistConfig = {
  key: 'plateapp',
  storage: AsyncStorage, // which reducer want to store
  whitelist: ['liked'],
};
const storageReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  storageReducer,
  applyMiddleware(thunk, apiMiddleware),
);
export const persistor = persistStore(store);
