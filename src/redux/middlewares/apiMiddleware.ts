const apiMiddleware = ({ dispatch, getState }: any) => (next: any) => (
  action: any,
) => {
  if (!action.api || !action.types) {
    return next(action);
  }
  const {
    api,
    types: [START, SUCCESS, ERROR],
    query,
  } = action;

  dispatch({
    type: START,
    query,
  });

  const { lang } = getState().lang;
  return api({ ...query, lang })
    .then((response: any) => {
      if (response && response.data && response.status === 200) {
        dispatch({
          type: SUCCESS,
          payload: response.data,
        });
        return {
          payload: response.data,
          query,
          responseStatus: response.status,
        };
      } else if (response.data.statusCode === 401) {
        throw { response };
      } else {
        throw (
          (response && response.data && { response }) || {
            response: { data: { message: 'Что-то не так!' } },
          }
        );
      }
    })
    .catch((error: any) => {
      dispatch({
        type: ERROR,
        error,
        query,
      });
      error.requestData = query;

      throw error;
    });
};

export default apiMiddleware;
