import React from 'react';
import styled from 'styled-components/native';
import global from './global';
import { IButtonProp, IMarginProp, IPaddingProp, ITextProp } from './types';

export const Margin = styled.View<IMarginProp>`
  margin: ${({ margin }) => (margin ? margin : '0px')};
  margin-left: ${({ left }) => (left ? left : '0px')};
  margin-right: ${({ right }) => (right ? right : '0px')};
  margin-top: ${({ top }) => (top ? top : '0px')};
  margin-bottom: ${({ bottom }) => (bottom ? bottom : '0px')};
`;
export const Padding = styled.View<IPaddingProp>`
  padding: ${({ padding }) => (padding ? padding : '0px')};
  padding-left: ${({ left }) => (left ? left : '0px')};
  padding-right: ${({ right }) => (right ? right : '0px')};
  padding-top: ${({ top }) => (top ? top : '0px')};
  padding-bottom: ${({ bottom }) => (bottom ? bottom : '0px')};
`;
export const HeaderWrap = styled.View`
  position: relative;
  z-index: 1;
  background-color: transparent;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 60px;
`;
export const TextFiled = styled.Text<ITextProp>`
  font-size: ${({ size = '16px' }) => size};
  color: ${({ color = global.colors.text }) => color};
  line-height: ${({ lineHeight = '24px' }) => lineHeight};
  font-family: ${({ weight = '400' }) =>
    weight == '700'
      ? global.fonts.bold
      : weight == '400'
      ? global.fonts.medium
      : global.fonts.semiBold};
`;

export const Clients = styled.TouchableOpacity`
  elevation: 5;
  width: 100%;
  height: 125px;
  margin-top: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-top-left-radius: 20px;
  border-top-right-radius: 100px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 100px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  overflow: hidden;
`;

export const ClientIcon = styled.View`
  position: relative;
  z-index: 1;
  width: 100px;
  height: 100px;
  background-color: white;
  border-radius: 50px;
  border-width: 1px;
  border-style: solid;
  border-color: #eee;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  text-align: center;
  elevation: 10;
`;

export const FoodMenu = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 10px 10px 5px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  height: 60px;
  margin-bottom: 10px;
  border-top-right-radius: 50px;
  border-top-left-radius: 50px;
  border-bottom-left-radius: 50px;
  border-bottom-right-radius: 50px;
  elevation: 10;
`;

export const CategoryImage = styled.View`
  align-items: center;
  justify-content: center;
  width: 60px;
  height: 60px;
  border-radius: 30px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08);
  background-color: #fff;
  elevation: 3;
`;

export const Drawer = styled.View`
  align-items: center;
  justify-content: space-between;
  width: 60px;
  height: 180px;
  border-radius: 30px;
  background-color: #fff;
  padding: 15px;
  z-index: 999;
`;

export const ArrowImage = styled.View`
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  border-radius: 20px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08);
  background-color: #fff;
  elevation: 3;
`;

export const ContentWrap = styled.View`
  elevation: 10;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  height: 44px;
  padding: 10px;
  margin-bottom: 10px;
  overflow: hidden;
`;

export const RestWrap = styled.View`
  flex: 1;
  elevation: 10;
  min-height: 400px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  width: 100%;
  height: 44px;
  padding: 20px 10px 10px;
  margin-bottom: 10px;
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
  margin-top: 10px;
  overflow: hidden;
`;
export const ImageShadow = styled.TouchableOpacity`
  position: relative;
  z-index: 1;
  width: 50px;
  height: 50px;
  align-items: center;
  justify-content: center;
  background-color: ${({ checked = false }) => (checked ? checked : '#fff')};
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.08);
  border-radius: 35px;
  border-color: gray;
  border-width: 0.3px;
`;

export const PlayerBg = styled.TouchableOpacity`
  width: 100px;
  height: 100px;
  background: #c4c4c4;
  border-radius: 50px;
  align-items: center;
  justify-content: center;
`;
export const Centered = styled.View`
  width: 100%;
  align-items: center;
  justify-content: center;
`;

export const ScrollLine = styled.View`
  /* position: absolute; */
  /* top: -15px; */
  /* left: 50%; */
  /* margin-right: 25px; */
  /* transform: translateX(-50%); */
  background-color: #000;
  width: 50px;
  height: 4px;
  border-radius: 2px;
  z-index: 1;
`;

export const ProfileInput = styled.TextInput`
  width: 100%;
  height: 50px;
  font-size: 14px;
  background: ${global.colors.secondaryColor};
  border-radius: 12px;
  padding: 0 38px 0 15px;
  color: ${global.colors.gray};
  text-decoration-line: none;
  font-family: ${global.fonts.medium};
`;

export const FilterButton = styled.TouchableOpacity`
  width: 50px;
  height: 50px;
  background-color: ${({ color = global.colors.secondaryColor }) => color};
  border-radius: 25px;
  align-items: center;
  justify-content: center;
  margin-left: 10px;
`;

export const FilterBlock = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  border-radius: 25px;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-bottom: 20px;
`;

export const FilterOverlay = styled.View`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 2;
  background: rgba(85, 78, 101, 0.8);
  align-items: center;
  justify-content: center;
  padding: 15px;
`;

export const FilterBlockButton = styled.TouchableOpacity`
  background: ${global.colors.secondaryColor};
  flex: 1;
  height: 40px;
  flex-direction: row;
  align-items: center;
  justify-content: ${({ center = false }) =>
    center ? 'center' : 'space-between'};
  width: ${({ width = 'auto' }) => `${width}`};
  padding: 0 10px;
  /* margin-bottom: 10px; */
  margin-left: 3px;
  margin-right: 3px;
  margin-bottom: 10px;
  flex-basis: 40%;
  border-radius: 12px;
`;

export const SearchButton = styled.TouchableOpacity`
  width: 100%;
  height: 50px;
  background: #dad6e1;
  border-radius: 25px;
  padding: 0px 35px 0px 15px;
  font-family: ${global.fonts.medium};
  justify-content: center;
`;

export const TextAreaClient = styled.TextInput`
  elevation: 5;
  width: 100%;
  height: 40px;
  background-color: white;
  font-size: 14px;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  padding: 0px 15px;
  margin-bottom: 10px;
`;

export const Button = styled.TouchableOpacity<IButtonProp>`
  flex: 1;
  height: 30px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0px 15px;
  border-radius: 10px;
  margin-top: auto;
  overflow: hidden;
`;
export const RegisterBlock = styled.ImageBackground`
  flex: 1;
  padding: 10px;
  justify-content: center;
  min-height: 600px;
  /* align-items: center; */
`;

export const InputBlock = styled.View`
  margin-bottom: 20px;
`;

export const RowBlock = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: ${({ wrap = 'nowrap' }) => wrap};
`;

export const OrderItem = styled.TouchableOpacity`
  elevation: 5;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  background-color: ${global.colors.white};
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
  height: 40px;
  margin-bottom: 10px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;

export const DatePickerWrap = styled.View`
  flex: 1;
  height: 30px;
  background-color: white;
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.12);
  border-radius: 10px;
  padding: 0px 15px;
`;

export const DynamicWrap = styled.View`
  flex-direction: ${({ dir = 'row' }) => dir};
  align-items: ${({ align = 'center' }) => align};
  width: ${({ width = '100%' }) => width};
  justify-content: ${({ justify = 'space-between' }) => justify};
`;

export const CenteredWrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: white;
  padding: 15px;
`;
export const SecondaryWrap = styled.View`
  width: 100%;
  min-height: 170px;
  background-color: ${global.colors.secondaryColor};
  align-items: center;
  justify-content: center;
  border-radius: 25px;
  padding: 20px 53px;
`;
