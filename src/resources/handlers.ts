export const durationTime = [
  { id: 10 },
  { id: 20 },
  { id: 30 },
  { id: 40 },
  { id: 50 },
  { id: 60 },
  { id: 90 },
  { id: 105 },
  { id: 120 },
  { id: 150 },
  { id: 165 },
  { id: 180 },
];

export const caloryArr = [
  { id: 50 },
  { id: 100 },
  { id: 150 },
  { id: 300 },
  { id: 500 },
  { id: 1000 },
  { id: 1500 },
  { id: 2000 },
  { id: 2500 },
  { id: 3000 },
  { id: 3500 },
  { id: 4000 },
];

export function timeConvert(n) {
  let num = n;
  let hours = num / 60;
  let rhours = Math.floor(hours);
  let minutes = (hours - rhours) * 60;
  let rminutes = Math.round(minutes);
  if (rminutes != 0) {
    return rhours + ' ' + rminutes;
  } else {
    let time = rhours + ' ' + rminutes;
    let b = time.slice(0, 1);
    return b;
  }
}
