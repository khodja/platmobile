const colors = {
  gray: '#9B9B9B',
  white: '#FFFFFF',
  blue: '#00A3FF',
  green: '#99BF40',
  text: '#3C3844',
  black: '#3C3844',
  mainTextColor: '#44485E',
  secondaryColor: '#F2F3F7',
  primaryColor: '#A90084',
  secondaryTextColor: '#5E6586',
};

export default colors;
