export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  TabThird: undefined;
  TabFourth: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};

export type ITextProp = {
  size?: string;
  color?: string;
  weight?: any;
  font?: string;
  lineHeight?: string;
};

export type IMarginProp = {
  margin?: string;
  left?: string;
  right?: string;
  top?: string;
  bottom?: string;
};

export type IPaddingProp = {
  padding?: string;
  left?: string;
  right?: string;
  top?: string;
  bottom?: string;
};
export type IButtonProp = {
  color?: any;
};
