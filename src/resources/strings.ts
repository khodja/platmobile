import { Dimensions } from 'react-native';
const { height, width } = Dimensions.get('window');
const strings = {
  name: 'Plate',
  width: width,
  height: height,
  blockSize: width - 40,
};

export default strings;
