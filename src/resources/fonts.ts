const fonts = {
  medium: 'Raleway-Medium',
  bold: 'Raleway-Bold',
  semiBold: 'Raleway-SemiBold',
};

export default fonts;
