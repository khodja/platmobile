import React from 'react';
import global from '../resources/global';
import Empty from '../../assets/images/error.svg';
import Again from '../../assets/images/again.svg';
import {
  CenteredWrapper,
  Padding,
  SecondaryWrap,
  TextFiled,
} from '../resources/styled';
import { useSelector } from 'react-redux';
import { langArr } from '../resources/lang';
import Scale from '../components/Scale';

export default function ErrorScreen({ reload = () => null }) {
  const { lang } = useSelector((state: any) => state.lang);
  return (
    <CenteredWrapper>
      <SecondaryWrap>
        <Padding bottom={10}>
          <Empty />
        </Padding>
        <TextFiled
          size="16px"
          weight={600}
          lineHeight="21px"
          style={{ textAlign: 'center' }}
          color={global.colors.mainTextColor}>
          {langArr[lang].error}
        </TextFiled>
      </SecondaryWrap>
      <Scale onPress={reload} style={{ marginTop: 50 }}>
        <Again />
      </Scale>
    </CenteredWrapper>
  );
}
