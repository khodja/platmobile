import React, { useCallback, useEffect, useMemo } from 'react';
import { View, Image, Linking, Share, Platform } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Layout from '../components/Layout';
import global from '../resources/global';
import { RowBlock, TextFiled, DynamicWrap } from '../resources/styled';
import Telegram from '../../assets/images/telegram.svg';
import Facebook from '../../assets/images/facebook.svg';
import Instagram from '../../assets/images/instagram.svg';
import Heart from '../../assets/images/heart.svg';
import ActiveHeart from '../../assets/images/redheart.svg';
import Player from '../../assets/images/play.svg';
import ShareIcon from '../../assets/images/share.svg';
import Views from '../../assets/images/views.svg';
import Step from '../components/Step';
import { useDispatch, useSelector } from 'react-redux';
import LoadingScreen from './LoadingScreen';
import {
  addLike,
  incrementView,
  removeLike,
} from '../redux/actions/likeAction';
import { langArr } from '../resources/lang';
import {
  RewardedAd,
  RewardedAdEventType,
  TestIds,
} from '@react-native-firebase/admob';
import Scale from '../components/Scale';
import TitleBlock from '../components/TitleBlock';
import { useApi } from '../utils/apiHook';
import { getReceiptByIdApi } from '../api/receiptApi';
import { useNavigation } from '@react-navigation/native';

const getYoutubeId = (url: string) => {
  const regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  const match = url.match(regExp);
  return match && match[2].length == 11 ? match[2] : false;
};
const SocialBlock = ({ facebook = null, instagram = null }) => (
  <RowBlock style={{ justifyContent: 'flex-start', paddingBottom: 16 }}>
    {facebook && (
      <>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(facebook);
          }}>
          <Facebook />
        </TouchableOpacity>
      </>
    )}
    {instagram && (
      <>
        <View style={{ width: 10 }} />
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(instagram);
          }}>
          <Instagram />
        </TouchableOpacity>
      </>
    )}
  </RowBlock>
);
export const handleObjectLang = (obj: object, prefix: string, lang: Number) => {
  const langName = lang === 0 ? 'uz' : lang === 1 ? 'ru' : 'en';
  return obj[`${prefix}_${langName}`];
};
const PrimaryText = ({ title }) => (
  <TextFiled size="16px" color={global.colors.primaryColor}>
    {title}
  </TextFiled>
);
export default function InfoScreen({ route }) {
  const navigation: any = useNavigation();
  const { id } = route.params;
  const dispatch = useDispatch();
  const { data, viewedCount } = useSelector((state: any) => state.liked);
  const { lang } = useSelector((state: any) => state.lang);
  const { data: receipt, loading, error, reload } = useApi(getReceiptByIdApi, {
    id,
  });
  const onShare = useCallback(async () => {
    try {
      await Share.share({
        message: 'https://plate.uz/receipt/' + id,
      });
    } catch (error) {
      alert(error.message);
    }
  }, [id]);
  const isLiked = useMemo(() => {
    if (data?.length > 0) {
      const isLiked = data.find((like) => like.id == receipt?.id);
      return !!isLiked;
    }
    return false;
  }, [data, receipt]);
  const switchLike = useCallback(() => {
    if (!isLiked) dispatch(addLike(receipt));
    else dispatch(removeLike(receipt?.id));
  }, [receipt, isLiked, data]);
  const renderShare = useMemo(
    () => (
      <RowBlock
        style={{
          backgroundColor: global.colors.secondaryColor,
          borderRadius: 20,
          padding: 10,
          marginTop: 20,
        }}>
        <DynamicWrap width="auto" align="center" justify="flex-start">
          <Scale onPress={switchLike}>
            {isLiked ? <ActiveHeart /> : <Heart />}
          </Scale>
          <Scale onPress={onShare} style={{ marginLeft: 16 }}>
            <ShareIcon />
          </Scale>
        </DynamicWrap>
        <DynamicWrap width="auto" align="center" justify="flex-end">
          <TextFiled
            size="16px"
            lineHeight="18px"
            color={global.colors.mainTextColor}>
            {receipt?.views}
          </TextFiled>
          <Views style={{ marginLeft: 10 }} />
        </DynamicWrap>
      </RowBlock>
    ),
    [isLiked, receipt, data],
  );
  const renderPlayer = useMemo(() => {
    if (!receipt?.video_url) return null;
    return (
      <RowBlock style={{ position: 'relative' }}>
        <Image
          source={{ uri: receipt?.image }}
          style={{
            borderRadius: 20,
            width: '100%',
            maxWidth: global.strings.blockSize,
            minHeight: 250,
            resizeMode: 'cover',
          }}
        />
        <View
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            backgroundColor: 'rgba(0,0,0,0.7)',
            borderRadius: 20,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Scale onPress={() => Linking.openURL(receipt.video_url)}>
            <Player />
          </Scale>
        </View>
      </RowBlock>
    );
  }, [receipt]);
  useEffect(() => {
    dispatch(incrementView());
    if (viewedCount % 4 === 0 && viewedCount !== 0) {
      const rewardId =
        Platform.OS === 'ios'
          ? 'ca-app-pub-8299324069750466/9090264295'
          : 'ca-app-pub-8299324069750466/7266579568';
      const rewarded = RewardedAd.createForAdRequest(rewardId, {
        requestNonPersonalizedAdsOnly: false,
        keywords: ['fashion', 'clothing'],
      });
      const eventListener = rewarded.onAdEvent((type) => {
        if (type === RewardedAdEventType.LOADED) {
          rewarded.show();
        }
      });
      rewarded.load();
      return () => eventListener();
    }
  }, []);

  if (loading || error || !receipt)
    return <LoadingScreen error={error} reload={reload} />;
  return (
    <Layout enableBack disableRight>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <Image
          source={{ uri: receipt?.image }}
          style={{
            borderRadius: 10,
            width: global.strings.blockSize,
            height: global.strings.blockSize,
            resizeMode: 'contain',
          }}
        />
        {renderShare}
        <View style={{ marginVertical: 20 }}>
          <View
            style={{
              borderRadius: 20,
              padding: 10,
              backgroundColor: global.colors.white,
              borderColor: global.colors.secondaryColor,
              borderWidth: 2,
            }}>
            <TextFiled
              color={global.colors.mainTextColor}
              weight={700}
              size="20px"
              style={{ paddingBottom: 20 }}>
              {receipt?.title}
            </TextFiled>
            <View style={{ paddingBottom: 16 }}>
              <TextFiled
                size="16px"
                color={global.colors.primaryColor}
                style={{ paddingBottom: 8 }}>
                {langArr[lang].author}:
              </TextFiled>
              <Scale
                activeScale={0.99}
                onPress={() =>
                  navigation.replace('Author', {
                    title: receipt?.user?.name,
                    id: receipt?.user?.id,
                    isAuthor: true,
                  })
                }>
                <TextFiled size="16px" color={global.colors.mainTextColor}>
                  {receipt?.user?.name}
                </TextFiled>
              </Scale>
            </View>
            <SocialBlock
              facebook={receipt?.user?.facebook}
              instagram={receipt?.user?.instagram}
            />
            <PrimaryText title={`${langArr[lang].category}: `} />
            <TextFiled size="16px" color={global.colors.mainTextColor}>
              {receipt?.categories?.map(
                (cat: any, index: Number) =>
                  handleObjectLang(cat, 'name', lang) +
                  (index != receipt?.categories.length - 1 ? ', ' : '.'),
              )}
            </TextFiled>
            <DynamicWrap style={{ marginTop: 15 }}>
              <View>
                <PrimaryText title={`${langArr[lang].calory}:`} />
                <TextFiled size="16px" color={global.colors.mainTextColor}>
                  {receipt?.calorie} kkal
                </TextFiled>
              </View>
              <View
                style={{
                  width: 1,
                  height: '70%',
                  backgroundColor: global.colors.secondaryColor,
                }}
              />
              <View>
                <PrimaryText title={`${langArr[lang].time}:`} />
                <TextFiled size="16px" color={global.colors.mainTextColor}>
                  {receipt?.time} {langArr[lang].min}
                </TextFiled>
              </View>
            </DynamicWrap>
          </View>
        </View>
        <View
          style={{
            marginTop: 15,
          }}>
          <RowBlock style={{ marginBottom: 19 }}>
            <TextFiled size="24px" weight={700} lineHeight="28px">
              {langArr[lang].ingridients}
            </TextFiled>
          </RowBlock>
          <View style={{ marginBottom: 15 }}>
            {receipt?.ingredients?.map((ingredient: any, key) => (
              <TextFiled
                size="12px"
                key={key}
                color={global.colors.gray}
                style={{ marginBottom: 5 }}>
                {ingredient?.content}
              </TextFiled>
            ))}
          </View>
          {receipt?.stages?.map((stage: any, key) => (
            <React.Fragment key={key}>
              <TitleBlock
                title={`${langArr[lang].step} ${key + 1}`}
                marginTop={20}
              />
              <Step stage={stage} />
            </React.Fragment>
          ))}
        </View>
        {receipt?.video_url && <TitleBlock title={`Video`} marginTop={70} />}
        {renderPlayer}
      </ScrollView>
    </Layout>
  );
}
