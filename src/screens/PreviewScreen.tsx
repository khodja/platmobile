import React, { useCallback, useMemo, useState } from 'react';
import { View } from 'react-native';
import Layout from '../components/Layout';
import PrevFirst from '../../assets/images/prev1.svg';
import PrevSecond from '../../assets/images/prev2.svg';
import PrevThird from '../../assets/images/prev3.svg';
import { TextFiled } from '../resources/styled';
import global from '../resources/global';
import Scale from '../components/Scale';
import { langArr } from '../resources/lang';
import { useDispatch, useSelector } from 'react-redux';
import { setReview } from '../redux/actions/likeAction';
import PreviewSettingScreen from './PreviewSettingScreen';

const calcWidth = global.strings.width * 0.8;
const iconProps = {
  width: calcWidth,
  height: calcWidth,
};
export default function PreviewScreen({ navigation }) {
  const { lang } = useSelector((state: any) => state.lang);
  const dispatch = useDispatch();
  const [stage, setStage] = useState<number>(-1);
  const stages = useMemo(
    () => [
      {
        title: langArr[lang].title1,
        description: langArr[lang].description1,
      },
      {
        title: langArr[lang].title2,
        description: langArr[lang].description2,
      },
      {
        title: langArr[lang].title3,
        description: langArr[lang].description3,
      },
    ],
    [lang],
  );
  const renderIcon = useMemo(() => {
    if (stage === 0) return <PrevFirst {...iconProps} />;
    if (stage === 1) return <PrevSecond {...iconProps} />;
    if (stage === 2) return <PrevThird {...iconProps} />;
  }, [stage]);

  const handleBack = useCallback(() => {
    if (stage > 0) setStage(stage - 1);
  }, [stage]);

  const handleNext = useCallback(() => {
    if (stage === 2) {
      dispatch(setReview());
      navigation.replace('Home');
    } else setStage(stage + 1);
  }, [stage]);

  if (stage === -1) {
    return <PreviewSettingScreen nextAction={handleNext} />;
  }

  return (
    <Layout disableHeader>
      <View
        style={{
          height: '100%',
          width: '100%',
          flex: 1,
          paddingTop: 50,
        }}>
        {renderIcon}
        <View style={{ justifyContent: 'space-between', flex: 1 }}>
          <View
            style={{
              alignItems: 'center',
              paddingTop: 20,
            }}>
            <TextFiled style={{ textAlign: 'center' }} weight="700" size="24px">
              {stages[stage].title}
            </TextFiled>
            <TextFiled
              style={{
                textAlign: 'center',
                paddingTop: 30,
              }}
              weight="400">
              {stages[stage].description}
            </TextFiled>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Scale
            activeScale={0.96}
            disabled={stage === 0}
            style={{ opacity: stage === 0 ? 0 : 1 }}
            onPress={handleBack}>
            <TextFiled weight="bold" color={global.colors.gray}>
              {langArr[lang].prev}
            </TextFiled>
          </Scale>
          <Scale activeScale={0.96} onPress={handleNext}>
            <TextFiled weight="bold" color={global.colors.primaryColor}>
              {langArr[lang].next}
            </TextFiled>
          </Scale>
        </View>
      </View>
    </Layout>
  );
}
