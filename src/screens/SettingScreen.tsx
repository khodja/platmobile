import React, { useCallback, useState } from 'react';
import Layout from '../components/Layout';
import global from '../resources/global';
import { RowBlock, TextFiled } from '../resources/styled';
import Scale from '../components/Scale';
import Lang from '../components/Lang';
import { useDispatch, useSelector } from 'react-redux';
import { changeLang } from '../redux/actions/langAction';
import { langArr } from '../resources/lang';
import { getReceiptList } from '../redux/actions/receiptAction';
import LoadingScreen from './LoadingScreen';
import TitleBlock from '../components/TitleBlock';
const languages = [
  { title: 'O`zbekcha', id: 0 },
  { title: 'Русский', id: 1 },
  { title: 'English', id: 2 },
];

export default function SettingScreen(props: any) {
  const { lang } = useSelector((state: any) => state.lang);
  const { loading } = useSelector((state: any) => state.receipt);
  const [active, setActive] = useState(lang);
  const dispatch = useDispatch();

  const saveLanguage = useCallback(() => {
    dispatch(changeLang(active));
    dispatch(getReceiptList());
  }, [active]);
  if (loading) return <LoadingScreen />;
  return (
    <Layout enableBack disableRight>
      <TitleBlock title={langArr[lang].settings} />
      {languages.map((lang, index) => (
        <Lang
          lang={lang}
          key={`land_${index}`}
          onClick={() => setActive(lang.id)}
          active={active}
        />
      ))}
      <Scale
        activeScale={0.94}
        onPress={saveLanguage}
        style={{ marginTop: 'auto', width: '100%', marginBottom: 15 }}>
        <RowBlock
          style={{
            backgroundColor: global.colors.white,
            borderRadius: 25,
            height: 50,
            marginTop: 15,
            justifyContent: 'center',
          }}>
          <TextFiled weight={'700'} size="18px">
            {langArr[lang].save}
          </TextFiled>
        </RowBlock>
      </Scale>
    </Layout>
  );
}
