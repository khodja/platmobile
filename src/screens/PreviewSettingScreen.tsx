import React, { useCallback, useState } from 'react';
import Layout from '../components/Layout';
import global from '../resources/global';
import { RowBlock, TextFiled } from '../resources/styled';
import Scale from '../components/Scale';
import Lang from '../components/Lang';
import { useDispatch, useSelector } from 'react-redux';
import { changeLang } from '../redux/actions/langAction';
import { langArr } from '../resources/lang';
import { getReceiptList } from '../redux/actions/receiptAction';
import LoadingScreen from './LoadingScreen';
import TitleBlock from '../components/TitleBlock';
const languages = [
  { title: 'O`zbekcha', id: 0 },
  { title: 'Русский', id: 1 },
  { title: 'English', id: 2 },
];

export default function PreviewSettingScreen({ nextAction = () => null }) {
  const { lang } = useSelector((state: any) => state.lang);

  const [active, setActive] = useState(lang);

  const dispatch = useDispatch();

  const saveLanguage = useCallback(() => {
    dispatch(changeLang(active));

    if (nextAction) nextAction();
  }, [active, nextAction]);

  return (
    <Layout disableHeader>
      <TitleBlock title={'Выберите язык'} />
      {languages.map((lang, index) => (
        <Lang
          lang={lang}
          key={`land_${index}`}
          onClick={() => setActive(lang.id)}
          active={active}
        />
      ))}
      <Scale
        activeScale={0.96}
        onPress={saveLanguage}
        style={{ marginTop: 'auto', width: '100%', marginBottom: 15 }}>
        <RowBlock
          style={{
            backgroundColor: global.colors.white,
            borderWidth: 1,
            borderColor: global.colors.gray,
            borderRadius: 25,
            height: 50,
            marginTop: 15,
            justifyContent: 'center',
          }}>
          <TextFiled weight={'700'} size="18px">
            {langArr[lang].next}
          </TextFiled>
        </RowBlock>
      </Scale>
    </Layout>
  );
}
