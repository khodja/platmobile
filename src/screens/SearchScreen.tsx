import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  View,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Layout from '../components/Layout';
import {
  ProfileInput,
  RowBlock,
  TextFiled,
  FilterBlock,
  FilterBlockButton,
} from '../resources/styled';
import Search from '../../assets/images/search-arrow.svg';
import Calory from '../../assets/images/calory.svg';
import Clock from '../../assets/images/clock.svg';
import Refresh from '../../assets/images/refresh.svg';
import Category from '../../assets/images/category.svg';
import Food from '../components/Food';
import global from '../resources/global';
import { useDispatch, useSelector } from 'react-redux';
import { search } from '../redux/actions/searchAction';
import { langArr } from '../resources/lang';
import { getCategoryList } from '../redux/actions/categoryAction';
import { durationTime, caloryArr } from '../resources/handlers';
import Filter from '../components/Filter';
import {
  chooseFilter,
  openFilter,
  resetFilter,
} from '../redux/actions/filterAction';
import NoResult from '../components/NoResult';
import ErrorScreen from './ErrorScreen';
import { handleObjectLang } from './InfoScreen';

const ChoiceBlock = ({ title = '', onPress = () => null, chosen = false }) => {
  return (
    <FilterBlockButton center onPress={onPress}>
      <TextFiled
        size="12px"
        color={
          chosen ? global.colors.primaryColor : global.colors.mainTextColor
        }>
        {title}
      </TextFiled>
    </FilterBlockButton>
  );
};
export default function SearchScreen() {
  const [value, setValue] = useState<any>('');
  const [page, setPage] = useState<number>(1);
  const dispatch = useDispatch();
  const { categories } = useSelector((state: any) => state.category);
  const { lang } = useSelector((state: any) => state.lang);
  const { receipts, loading, error, hasMore, moreLoading } = useSelector(
    (state: any) => state.search,
  );
  const {
    filter,
    calory,
    caloryOpen,
    duration,
    durationOpen,
    category,
    categoryOpen,
  } = useSelector((state: any) => state.filter);
  const findCategory = useMemo(() => {
    if (category) {
      const result = categories?.find((cat) => cat.id === category);
      if (result) {
        return handleObjectLang(result, 'name', lang);
      }
    }
    return '';
  }, [categories, category, lang]);
  const renderFilter = useMemo(
    () =>
      [
        {
          title: !!calory ? `${calory} kkal` : langArr[lang].calory,
          icon: <Calory />,
          onPress: () => dispatch(openFilter('calory')),
          chosen: !!calory,
        },
        {
          title: !!duration ? `${duration} min` : langArr[lang].time,
          icon: <Clock />,
          onPress: () => dispatch(openFilter('duration')),
          chosen: !!duration,
        },
        {
          title: !!category ? findCategory : langArr[lang].category,
          icon: <Category />,
          onPress: () => dispatch(openFilter('category')),
          chosen: !!category,
        },
        {
          title: langArr[lang].reset,
          icon: <Refresh />,
          onPress: () => {
            dispatch(resetFilter());
            setPage(1);
          },
          chosen: null,
        },
      ].map((item, key) => {
        return <ChoiceBlock key={`choice_${key}`} {...item} />;
      }),
    [
      lang,
      categoryOpen,
      caloryOpen,
      durationOpen,
      calory,
      category,
      duration,
      categories,
    ],
  );

  const getData = useCallback(() => {
    dispatch(
      search({
        ...(value.trim() !== '' && { keyword: value }),
        ...(category && { category_id: category }),
        ...(calory && { calories: calory }),
        ...(duration && { time: duration }),
        page,
      }),
    );
  }, [value, category, calory, duration, page]);

  const addCaloryFilter = useCallback((value) => {
    dispatch(chooseFilter({ key: 'calory', value }));
  }, []);

  const addCategoryFilter = useCallback((value) => {
    dispatch(chooseFilter({ key: 'category', value }));
  }, []);

  const addDurationFilter = useCallback((value) => {
    dispatch(chooseFilter({ key: 'duration', value }));
  }, []);
  const renderLayers = useMemo(() => {
    return (
      <>
        {caloryOpen && (
          <Filter suffix="kkal" data={caloryArr} onPress={addCaloryFilter} />
        )}
        {durationOpen && (
          <Filter data={durationTime} isTime onPress={addDurationFilter} />
        )}
        {categoryOpen && (
          <Filter data={categories} isCategory onPress={addCategoryFilter} />
        )}
      </>
    );
  }, [caloryOpen, categoryOpen, durationOpen]);
  const renderResult = useMemo(() => {
    if (loading) {
      return (
        <ActivityIndicator color={global.colors.primaryColor} size="large" />
      );
    }
    if (!receipts.length) return <NoResult />;
    return (
      <FlatList
        data={receipts}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => <Food item={item} />}
        onEndReachedThreshold={0.1}
        onEndReached={({ distanceFromEnd }) => {
          if (distanceFromEnd < 200) {
            if (hasMore && !moreLoading) setPage(page + 1);
          }
        }}
        keyExtractor={(item) => item?.id}
        ListFooterComponent={() =>
          moreLoading && (
            <ActivityIndicator
              key="footer"
              color={global.colors.primaryColor}
              size="large"
            />
          )
        }
      />
    );
  }, [loading, receipts, moreLoading, hasMore]);

  useEffect(() => {
    if (!categories.length) dispatch(getCategoryList());
  }, []);

  useEffect(() => {
    getData();
  }, [category, calory, duration, page]);

  if (error) return <ErrorScreen reload={getData} />;
  return (
    <Layout enableBack enableFilter>
      <View style={{ height: 10 }} />
      {filter && (
        <FilterBlock>
          <RowBlock style={{ flexWrap: 'wrap' }}>{renderFilter}</RowBlock>
        </FilterBlock>
      )}
      <RowBlock style={{ marginBottom: 20 }}>
        <View
          style={{
            flex: 1,
            position: 'relative',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ProfileInput
            placeholder="Поиск"
            placeholderTextColor={global.colors.gray}
            value={value}
            onBlur={getData}
            onChangeText={setValue}
          />
          <View
            style={{
              position: 'absolute',
              zIndex: 1,
              right: 15,
              justifyContent: 'center',
            }}>
            <TouchableOpacity onPress={getData}>
              <Search width={16} />
            </TouchableOpacity>
          </View>
        </View>
      </RowBlock>
      <View style={{ flex: 1, marginBottom: 20 }}>{renderResult}</View>
      {renderLayers}
    </Layout>
  );
}
