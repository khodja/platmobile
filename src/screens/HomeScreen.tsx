import React, { useCallback, useEffect, useMemo } from 'react';
import { RefreshControl, ScrollView } from 'react-native';
import Layout from '../components/Layout';
import { RowBlock } from '../resources/styled';
import Food from '../components/Food';
import { connect } from 'react-redux';
import LoadingScreen from './LoadingScreen';
import { getReceiptList } from '../redux/actions/receiptAction';
import { langArr } from '../resources/lang';
import { configureAdmob } from '../ads/requestMethod';
import CategoryItem from '../components/CategoryItem';
import { getCategoryList } from '../redux/actions/categoryAction';
import TitleBlock from '../components/TitleBlock';
import { Banner } from '../components/BannerAdmob';

interface IHomeScreenProps {
  categories: any;
  getSlider: any;
  getReceiptList: () => null;
  getCategoryList: () => null;
  slider: Array<any>;
  sliderLoading: boolean;
  sliderError: string | null;
  receipts: Array<any>;
  receiptLoading: boolean;
  categoryLoading: boolean;
  receiptError: string | null;
  lang: number;
}

const HomeScreen = (props: IHomeScreenProps) => {
  const renderCategories = useMemo(() => {
    if (props.categoryLoading) return null;
    return (
      <RowBlock wrap="wrap">
        {props.categories.map((cat) => (
          <CategoryItem data={cat} key={`${cat.id}_category`} />
        ))}
      </RowBlock>
    );
  }, [props.categories, props.categoryLoading]);

  const renderReceipts = useMemo(() => {
    if (props.receiptLoading) return null;
    return props?.receipts?.map((item) => (
      <Food key={`food_${item.id}`} item={item} />
    ));
  }, [props.receipts, props.receiptLoading]);

  const loadData = useCallback(
    (hasCategory = true) => {
      props.getReceiptList();
      if (hasCategory) props.getCategoryList();
    },
    [props.receiptError, props.sliderError],
  );

  useEffect(() => {
    configureAdmob();
    loadData();
  }, []);
  if (props.receiptLoading || props.receiptError || props.sliderError)
    return (
      <LoadingScreen
        error={props.sliderError || props.receiptError}
        reload={loadData}
      />
    );

  return (
    <Layout>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={props.receiptLoading}
            onRefresh={() => {
              () => loadData(false);
            }}
          />
        }>
        <TitleBlock title={langArr[props.lang].popularCategory} />
        {renderCategories}
        <Banner />
        <TitleBlock title={langArr[props.lang].popular} />
        {renderReceipts}
      </ScrollView>
    </Layout>
  );
};

function mapStateToProps(state) {
  const { slider, loading: sliderLoading, error: sliderError } = state.slider;
  const { receipts, loading, error } = state.receipt;
  const {
    categories,
    loading: categoryLoading,
    error: categoryError,
  } = state.category;
  const { lang } = state.lang;
  return {
    slider: slider,
    sliderLoading,
    sliderError,
    receipts: receipts,
    receiptLoading: loading,
    receiptError: error,
    lang,
    categories: categories.slice(0, 4),
    categoryLoading,
    categoryError,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    getReceiptList: () => dispatch(getReceiptList()),
    getCategoryList: () => dispatch(getCategoryList()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
