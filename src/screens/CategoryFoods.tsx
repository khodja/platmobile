import React from 'react';
import { ActivityIndicator, FlatList } from 'react-native';
import Layout from '../components/Layout';
import Food from '../components/Food';
import LoadingScreen from './LoadingScreen';
import TitleBlock from '../components/TitleBlock';
import { getCategoryByIdApi, getFoodByAuthorApi } from '../api/categoryApi';
import { useApiPaginate } from '../utils/apiHook';
import { useSelector } from 'react-redux';
import global from '../resources/global';

export default function CategoryFood({ route }) {
  const { id, image = null, title, isAuthor = false } = route.params;
  const { lang } = useSelector((state: any) => state.lang);
  const {
    error,
    data,
    loading,
    reload,
    hasMore,
    moreLoading,
    nextPage,
  } = useApiPaginate(isAuthor ? getFoodByAuthorApi : getCategoryByIdApi, {
    id,
    lang,
  });
  if (loading || !data || !!error) {
    return <LoadingScreen error={error} reload={reload} />;
  }

  return (
    <Layout enableBack>
      <TitleBlock title={title} image={image} />
      <FlatList
        data={data}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => <Food item={item} />}
        onEndReachedThreshold={0.1}
        onEndReached={({ distanceFromEnd }) => {
          if (distanceFromEnd < 200) {
            if (hasMore && !moreLoading) nextPage();
          }
        }}
        keyExtractor={(item) => item?.id}
        ListFooterComponent={() =>
          moreLoading && (
            <ActivityIndicator
              color={global.colors.primaryColor}
              size="large"
            />
          )
        }
      />
    </Layout>
  );
}
