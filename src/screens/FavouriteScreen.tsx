import React from 'react';
import { FlatList } from 'react-native';
import Layout from '../components/Layout';
import Food from '../components/Food';
import { useSelector } from 'react-redux';
import { langArr } from '../resources/lang';
import TitleBlock from '../components/TitleBlock';

export default function FavouriteScreen(props: any) {
  const { data } = useSelector((state: any) => state.liked);
  const { lang } = useSelector((state: any) => state.lang);

  return (
    <Layout enableBack>
      <TitleBlock title={langArr[lang].favourite} />
      <FlatList
        data={data}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => <Food key={item.id} item={item} />}
        keyExtractor={(item) => item?.id}
      />
    </Layout>
  );
}
