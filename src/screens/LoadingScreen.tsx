import React from 'react';
import { View } from 'react-native';
import global from '../resources/global';
import Logo from '../../assets/images/logo.svg';
import ErrorScreen from './ErrorScreen';
const LoadingScreen = ({ error = null, reload = () => null }) => {
  if (error) return <ErrorScreen reload={reload} />;
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: global.colors.white,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Logo />
    </View>
  );
};
export default LoadingScreen;
