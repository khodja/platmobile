import React, { useCallback, useEffect } from 'react';
import { FlatList } from 'react-native';
import Layout from '../components/Layout';
import CategoryBlock from '../components/CategoryBlock';
import { useDispatch, useSelector } from 'react-redux';
import { getCategoryList } from '../redux/actions/categoryAction';
import LoadingScreen from './LoadingScreen';
import { langArr } from '../resources/lang';
import TitleBlock from '../components/TitleBlock';

export default function CategoryScreen() {
  const dispatch = useDispatch();

  const { categories, loading, error } = useSelector(
    (state: any) => state.category,
  );

  const { lang } = useSelector((state: any) => state.lang);

  const getData = useCallback(() => {
    dispatch(getCategoryList());
  }, []);

  useEffect(() => {
    if (!categories.length) getData();
  }, []);

  if (loading || !categories || error) {
    return <LoadingScreen error={error} reload={getData} />;
  }

  return (
    <Layout enableBack>
      <TitleBlock title={langArr[lang].allCategories} />
      <FlatList
        data={categories}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => (
          <CategoryBlock key={item.id} category={item} />
        )}
        keyExtractor={(item) => item?.id}
      />
    </Layout>
  );
}
