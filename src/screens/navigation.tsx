import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import InfoScreen from './InfoScreen';
import CategoryScreen from './CategoryScreen';
import SettingScreen from './SettingScreen';
import FavouriteScreen from './FavouriteScreen';
import CategoryFood from './CategoryFoods';
import LoadingScreen from './LoadingScreen';
import PreviewScreen from './PreviewScreen';
import { useSelector } from 'react-redux';
import global from '../resources/global';
import MainDrawer from '../components/MainDrawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const deepLinking: any = {
  enabled: true,
  prefixes: ['https://plate.uz/', 'com.plate://'],
  config: {
    screens: {
      Info: 'receipt/:id',
    },
  },
};

const MainStack = () => (
  <Drawer.Navigator
    screenOptions={{
      drawerStyle: { width: global.strings.width / 2 },
      headerShown: false,
    }}
    drawerContent={({ navigation }) => <MainDrawer navigation={navigation} />}>
    <Drawer.Screen name="Main" component={HomeScreen} />
  </Drawer.Navigator>
);

export default function AppContainer() {
  const { reviewPassed } = useSelector((state: any) => state.liked);
  return (
    <NavigationContainer linking={deepLinking} fallback={<LoadingScreen />}>
      <Stack.Navigator initialRouteName={reviewPassed ? 'Home' : 'Preview'}>
        <Stack.Screen
          name="Preview"
          options={{ headerShown: false }}
          component={PreviewScreen}
        />
        <Stack.Screen
          name="Home"
          options={{ headerShown: false }}
          component={MainStack}
        />
        <Stack.Screen
          name="Search"
          options={{ headerShown: false }}
          component={SearchScreen}
        />
        <Stack.Screen
          name="Loading"
          options={{ headerShown: false }}
          component={LoadingScreen}
        />
        <Stack.Screen
          name="Info"
          options={{ headerShown: false }}
          component={InfoScreen}
        />
        <Stack.Screen
          name="Category"
          options={{ headerShown: false }}
          component={CategoryScreen}
        />
        <Stack.Screen
          name="Settings"
          options={{ headerShown: false }}
          component={SettingScreen}
        />
        <Stack.Screen
          name="Liked"
          options={{ headerShown: false }}
          component={FavouriteScreen}
        />
        <Stack.Screen
          name="CategoryFood"
          options={{ headerShown: false }}
          component={CategoryFood}
        />
        <Stack.Screen
          name="Author"
          options={{ headerShown: false }}
          component={CategoryFood}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
