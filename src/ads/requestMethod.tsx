import admob, { MaxAdContentRating } from '@react-native-firebase/admob';

export const configureAdmob = () =>
  admob().setRequestConfiguration({
    // Update all future requests suitable for parental guidance
    maxAdContentRating: MaxAdContentRating.G,
    tagForChildDirectedTreatment: false,
    tagForUnderAgeOfConsent: true,
  });
