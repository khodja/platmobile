import axios from 'axios';
import { HttpConfig } from '../api/httpConfig';

export default class HttpClient {
  static headers: any = {
    'Content-Type': 'application/json',
  };

  /**@private*/
  static async doRequest() {
    HttpClient.headers = {
      ...HttpClient.headers,
      'Accept-Language': 'ru',
      Authorization: `Bearer plate.indev`,
    };
    return axios.create({
      headers: {
        ...HttpClient.headers,
      },
    });
  }

  static async doGet(url, params = {}) {
    return (await HttpClient.doRequest()).get(HttpConfig.API_PATH + url, {
      params,
    });
  }
}
