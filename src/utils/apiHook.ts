import axios from 'axios';
import { useEffect, useState } from 'react';

export const useApi = (api: any, body = {}) => {
  const ourRequest = axios.CancelToken.source();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [data, setData] = useState<any>(null);
  const [reload, setReload] = useState(0);
  useEffect(() => {
    const fetchData = async () => {
      api({ ...body, cancelToken: ourRequest.token })
        .then(({ data: response }) => {
          if (response) {
            setData(response?.data);
            if (!!error) setError(null);
          }
        })
        .catch((error) => {
          setError(error?.message);
        })
        .finally(() => {
          setLoading(false);
        });
    };
    fetchData();
    return () => {
      ourRequest.cancel();
    };
  }, [reload]);

  return { error, data, loading, reload: () => setReload(reload + 1) };
};
export const useApiPaginate = (api: any, body = {}) => {
  const ourRequest = axios.CancelToken.source();
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState(null);
  const [data, setData] = useState<any>([]);
  const [page, setPage] = useState<number>(1);
  const [hasMore, setHasMore] = useState<boolean>(false);
  const [moreLoading, setMoreLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      if (page > 1) setMoreLoading(true);
      api({ ...body, page, cancelToken: ourRequest.token })
        .then(({ data: response }) => {
          if (response) {
            if (page > 1) {
              setData([...data, ...response?.data]);
            } else {
              setData(response?.data);
            }
            setHasMore(!!response.next_page_url);
            if (error) setError(null);
          }
        })
        .catch((error) => {
          setError(error?.message || 'error');
        })
        .finally(() => {
          if (page > 1 || moreLoading) setMoreLoading(false);
          if (loading) setLoading(false);
        });
    };

    fetchData();

    return () => {
      ourRequest.cancel();
    };
  }, [page]);

  const nextPage = () => !moreLoading && hasMore && setPage(page + 1);
  const reload = () => !moreLoading && setPage(1);

  return {
    error,
    data,
    loading,
    reload,
    hasMore,
    moreLoading,
    nextPage,
  };
};
