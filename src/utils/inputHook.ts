import { useState } from 'react';

export function useCustonInput() {
  const [value, setValue] = useState('');
  const onChange = (text: string): void => {
    setValue(text);
  };
  return {
    value,
    onChange,
  };
}
