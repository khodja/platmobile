import React, { useEffect } from 'react';
import { Image, View } from 'react-native';
import Swiper from 'react-native-swiper';
import { useSelector, useDispatch } from 'react-redux';
import { getSlider } from '../redux/actions/sliderAction';
import global from '../resources/global';

const sliderHeight = Math.floor(global.strings.width / 3);

export default function Slider() {
  const { data = [] } = useSelector((state: any) => state.slider);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSlider());
  }, []);
  return (
    <View style={{ height: sliderHeight, marginBottom: 30 }}>
      <Swiper
        height={sliderHeight}
        showsPagination={false}
        dot={false}
        loop={true}
        autoplay
        autoplayTimeout={4}>
        {!!data &&
          data.map((slider: any, key: Number) => (
            <Image
              key={`${slider.id} ${key}`}
              style={{
                width: global.strings.width,
                height: sliderHeight,
                resizeMode: 'cover',
              }}
              source={{ uri: slider.image }}
            />
          ))}
      </Swiper>
    </View>
  );
}
