import React, { useMemo } from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Image } from 'react-native';
import global from '../resources/global';
import { TextFiled } from '../resources/styled';
import Scale from './Scale';
import { useSelector } from 'react-redux';
import { handleObjectLang } from '../screens/InfoScreen';

export default function Food({ item }) {
  const navigation: any = useNavigation();
  const { lang } = useSelector((state: any) => state.lang);
  const categoryName = useMemo(() => {
    return handleObjectLang(item?.categories[0], 'name', lang);
  }, [item]);
  return (
    <Scale
      activeScale={0.97}
      onPress={() => navigation.navigate('Info', { id: item.id })}
      style={{
        flexDirection: 'row',
        backgroundColor: global.colors.secondaryColor,
        width: '100%',
        alignItems: 'center',
        marginBottom: 8,
        minHeight: 90,
        padding: 12,
        borderRadius: 20,
      }}>
      <Image
        source={{ uri: item.image }}
        style={{ borderRadius: 15, width: 62, height: 62, marginRight: 10 }}
      />
      <View style={{ justifyContent: 'center', flex: 1 }}>
        <TextFiled weight={'600'} size="16px" lineHeight="19px">
          {item.title}
        </TextFiled>
        <TextFiled
          color={global.colors.gray}
          size="14px"
          style={{ paddingTop: 8 }}>
          {categoryName}
        </TextFiled>
      </View>
    </Scale>
  );
}
