import React from 'react';
import { View, Text } from 'react-native';
import global from '../resources/global';
import { RowBlock, TextFiled } from '../resources/styled';

export default function MenuHeader(props: any) {
  return (
    <RowBlock
      style={{
        paddingTop: 30,
        paddingBottom: 5,
        borderBottomWidth: 4,
        borderBottomColor: global.colors.white,
      }}>
      <TextFiled
        size="24px"
        weight={700}
        lineHeight="28px"
        color={global.colors.mainTextColor}>
        {props.title}
      </TextFiled>
    </RowBlock>
  );
}
