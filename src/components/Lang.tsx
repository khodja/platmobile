import React, { useMemo } from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import global from '../resources/global';
import { RowBlock, TextFiled } from '../resources/styled';
import Scale from './Scale';
import Done from '../../assets/images/chose_lang.svg';
import { View } from 'react-native';

export default function Lang({ lang, onClick, active }) {
  const isActive = useMemo(() => lang.id === active, [active, lang]);
  return (
    <Scale activeScale={0.98} style={{ marginBottom: 20 }} onPress={onClick}>
      <RowBlock
        style={{
          backgroundColor: global.colors.secondaryColor,
          borderRadius: 25,
          height: 44,
          paddingHorizontal: 15,
        }}>
        <TextFiled weight={'400'} size="18px">
          {lang.title}
        </TextFiled>

        {isActive ? (
          <Done />
        ) : (
          <View
            style={{
              width: 22,
              height: 22,
              borderRadius: 15,
              borderWidth: 2,
              borderColor: global.colors.gray,
            }}
          />
        )}
      </RowBlock>
    </Scale>
  );
}
