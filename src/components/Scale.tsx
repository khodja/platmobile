import React from 'react';
import TouchableScale from 'react-native-touchable-scale';

export default function Scale({
  onPress = () => null,
  activeScale = 0.85,
  style = {},
  children,
  disabled = false,
}) {
  return (
    <TouchableScale
      activeScale={activeScale}
      disabled={disabled}
      onPress={() => onPress()}
      style={style}>
      {children}
    </TouchableScale>
  );
}
