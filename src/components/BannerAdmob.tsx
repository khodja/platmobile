import React from 'react';
import { BannerAd, BannerAdSize } from '@react-native-firebase/admob';
import { View, Platform } from 'react-native';

const id =
  Platform.OS === 'ios'
    ? 'ca-app-pub-8299324069750466/4422253369 '
    : 'ca-app-pub-8299324069750466/6530274166';
export const Banner = () => (
  <View style={{ marginVertical: 30 }}>
    <BannerAd
      unitId={id}
      size={BannerAdSize.FULL_BANNER}
      requestOptions={{ requestNonPersonalizedAdsOnly: true }}
    />
  </View>
);
