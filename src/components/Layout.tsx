import React from 'react';
import Header from './Header';
import { SafeAreaView } from 'react-native-safe-area-context';
import global from '../resources/global';
import { StatusBar } from 'react-native';

export default function Layout({
  enableBack = false,
  children,
  disableHeader = false,
  enableFilter = false,
  enableSearch = true,
  disableRight = false,
}) {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: global.colors.white,
        paddingHorizontal: 20,
      }}>
      <StatusBar backgroundColor={'#fff'} barStyle={'light-content'} />
      {!disableHeader && (
        <Header
          enableFilter={enableFilter}
          enableBack={enableBack}
          enableSearch={enableSearch}
          disableRight={disableRight}
        />
      )}
      {children}
    </SafeAreaView>
  );
}
