import React from 'react';
import { StatusBar } from 'react-native';

export default function CustomStatusBar() {
  return <StatusBar barStyle={'light-content'} />;
}
