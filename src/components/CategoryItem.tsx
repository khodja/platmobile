import React, { useMemo } from 'react';
import { Image, View } from 'react-native';
import { useSelector } from 'react-redux';
import { TextFiled } from '../resources/styled';
import { handleObjectLang } from '../screens/InfoScreen';
import Scale from './Scale';
import styled from 'styled-components/native';
import global from '../resources/global';
import { useNavigation } from '@react-navigation/native';
import Layer from '../../assets/images/catlayer.svg';

const width = global.strings.width / 2.3;
const resultWidth = width > 400 ? 230 : width;

export default function CategoryItem({ data }) {
  const { lang } = useSelector((state: any) => state.lang);
  const navigation: any = useNavigation();
  const name = useMemo(() => handleObjectLang(data, 'name', lang), [lang]);
  return (
    <Scale
      style={{ marginBottom: 12 }}
      activeScale={0.94}
      onPress={() =>
        navigation.navigate('CategoryFood', {
          id: data.id,
          image: data.image,
          title: name,
        })
      }>
      <CategoryBlock>
        <TextFiled weight="600" style={{ paddingRight: 30 }} size={'16px'}>
          {name}
        </TextFiled>
        <Layer
          style={{
            width: '100%',
            position: 'absolute',
            bottom: 2,
            right: 2,
            zIndex: -1,
          }}
        />
        <View
          style={{
            alignItems: 'flex-end',
            position: 'absolute',
            bottom: 15,
            left: 0,
            right: 15,
          }}>
          <Image
            source={{ uri: data.image }}
            style={{ width: 60, height: 60, resizeMode: 'contain' }}
          />
        </View>
      </CategoryBlock>
    </Scale>
  );
}

const CategoryBlock = styled.View`
  width: ${resultWidth}px;
  height: ${resultWidth - 40}px;
  background-color: ${global.colors.secondaryColor};
  border-radius: 20px;
  padding: 15px;
`;
