import React, { useCallback } from 'react';
import { View, Text, Image } from 'react-native';
import global from '../resources/global';
import {
  ArrowImage,
  CategoryImage,
  RowBlock,
  TextFiled,
} from '../resources/styled';
import Scale from './Scale';
import Arrow from '../../assets/images/arrow.svg';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import { handleObjectLang } from '../screens/InfoScreen';
import { langArr } from '../resources/lang';
export default function CategoryBlock({ category }) {
  const navigation = useNavigation();
  const { lang } = useSelector((state: any) => state.lang);
  const name = handleObjectLang(category, 'name', lang);
  return (
    <Scale
      style={{ marginBottom: 15 }}
      activeScale={0.95}
      onPress={() =>
        navigation.navigate('CategoryFood', {
          id: category.id,
          image: category.image,
          title: name,
        })
      }>
      <RowBlock
        style={{
          borderRadius: 20,
          backgroundColor: global.colors.secondaryColor,
          paddingVertical: 18,
          paddingHorizontal: 24,
        }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={{ uri: category.image }}
            style={{ width: 40, height: 40, resizeMode: 'contain' }}
          />
          <View style={{ marginLeft: 10 }}>
            <TextFiled weight={'700'} size="14px">
              {name}
            </TextFiled>
            <TextFiled color={global.colors.gray} size="12px">
              {category.receipts_count}-{langArr[lang].receipts_count}
            </TextFiled>
          </View>
        </View>
      </RowBlock>
    </Scale>
  );
}
