import React, { useCallback, useMemo } from 'react';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import styled from 'styled-components/native';
import { HeaderWrap } from '../resources/styled';
import ArrowBack from '../../assets/images/arrow-back.svg';
import Menu from '../../assets/images/menu.svg';
import Logo from '../../assets/images/logo.svg';
import Search from '../../assets/images/search.svg';
import Filter from '../../assets/images/filter.svg';
import ActiveFilter from '../../assets/images/active-filter.svg';
import Scale from './Scale';
import { useDispatch, useSelector } from 'react-redux';
import { switchFilter } from '../redux/actions/filterAction';

export default ({ enableBack, enableFilter, enableSearch, disableRight }) => {
  const navigation: any = useNavigation();
  const { filter } = useSelector((state: any) => state.filter);
  const dispatch = useDispatch();
  const onPress = useCallback(() => {
    if (enableBack) navigation.goBack();
    else navigation.dispatch(DrawerActions.toggleDrawer());
  }, [enableBack, navigation]);
  const renderIcon = useMemo(() => {
    if (enableFilter) {
      if (filter) return <ActiveFilter />;
      return <Filter />;
    }
    if (enableSearch) return <Search />;
    return null;
  }, [enableFilter, enableSearch, filter]);
  const handlePress = () => {
    if (enableFilter) {
      dispatch(switchFilter());
      return;
    }
    if (enableSearch) return navigation.navigate('Search');
  };
  const goHome = useCallback(() => {
    return navigation.replace('Home');
  }, []);
  return (
    <HeaderWrap>
      <Centered>
        <Scale onPress={onPress}>{enableBack ? <ArrowBack /> : <Menu />}</Scale>
      </Centered>
      <Scale onPress={goHome}>
        <Logo />
      </Scale>
      <Centered align="flex-end">
        {!disableRight && <Scale onPress={handlePress}>{renderIcon}</Scale>}
      </Centered>
    </HeaderWrap>
  );
};

const Centered = styled.View`
  flex: 1;
  justify-content: center;
  align-items: ${({ align = 'flex-start' }) => align};
`;
