import React from 'react';
import { BlurView } from '@react-native-community/blur';
import {
  FilterOverlay,
  FilterBlock,
  FilterBlockButton,
  TextFiled,
} from '../resources/styled';
import global from '../resources/global';
import { useSelector } from 'react-redux';
import { timeConvert } from '../resources/handlers';
import { langArr } from '../resources/lang';
import { handleObjectLang } from '../screens/InfoScreen';

export default function Filter({
  data,
  onPress = (data: string) => null,
  suffix = '',
  isTime = false,
  isCategory = false,
}) {
  const { lang } = useSelector((state: any) => state.lang);
  return (
    <BlurView
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 2,
      }}
      blurType="light"
      blurAmount={10}
      reducedTransparencyFallbackColor="white">
      <FilterOverlay blurRadius={90}>
        <FilterBlock>
          {data.map((item: any) => {
            let renderName = `${item.id} ${suffix}`;
            if (isTime) {
              let time = item.id < 60 ? item.id : timeConvert(item.id);
              {
                renderName =
                  item.id < 60
                    ? `${time} ${langArr[lang].min}`
                    : `${time} ${langArr[lang].hour}`;
              }
            }
            if (isCategory) {
              renderName = handleObjectLang(item, 'name', lang);
            }
            return (
              <FilterBlockButton
                center={true}
                key={item.id}
                onPress={() => onPress(item.id)}>
                <TextFiled size="12px" color={global.colors.gray}>
                  {renderName}
                </TextFiled>
              </FilterBlockButton>
            );
          })}
        </FilterBlock>
      </FilterOverlay>
    </BlurView>
  );
}
