import React from 'react';
import { View, Text } from 'react-native';
import styled from 'styled-components/native';
import global from '../resources/global';
import Empty from '../../assets/images/search-empty.svg';
import {
  CenteredWrapper,
  Padding,
  SecondaryWrap,
  TextFiled,
} from '../resources/styled';
import { useSelector } from 'react-redux';
import { langArr } from '../resources/lang';

export default function NoResult() {
  const { lang } = useSelector((state: any) => state.lang);
  return (
    <CenteredWrapper>
      <SecondaryWrap>
        <Padding bottom={10}>
          <Empty />
        </Padding>
        <TextFiled
          size="16px"
          weight={600}
          lineHeight="21px"
          style={{ textAlign: 'center' }}
          color={global.colors.mainTextColor}>
          {langArr[lang].empty}
        </TextFiled>
      </SecondaryWrap>
    </CenteredWrapper>
  );
}
