import React from 'react';
import { View, Image } from 'react-native';
import global from '../resources/global';
import { TextFiled } from '../resources/styled';

export default function Step({ stage }) {
  return (
    <View>
      <TextFiled size="14px" color={global.colors.gray}>
        {stage.content}
      </TextFiled>
      <Image
        source={{ uri: stage.image }}
        style={{
          minHeight: global.strings.blockSize / 1.675,
          resizeMode: 'contain',
          borderRadius: 10,
          marginTop: 20,
        }}
      />
    </View>
  );
}
