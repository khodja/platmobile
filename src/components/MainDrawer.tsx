import React, { useMemo } from 'react';
import { View, Text, Linking } from 'react-native';
import { useSelector } from 'react-redux';
import { langArr } from '../resources/lang';
import styled from 'styled-components/native';
import { TextFiled } from '../resources/styled';
import Settings from '../../assets/images/setting.svg';
import Category from '../../assets/images/category.svg';
import Liked from '../../assets/images/redheart.svg';
import Indev from '../../assets/images/indev.svg';
import global from '../resources/global';
import Scale from './Scale';

const drawerList = (lang: string, navigation: any) => [
  {
    name: langArr[lang].favourite,
    onPress: () => navigation.navigate('Liked'),
    Icon: Liked,
  },
  {
    name: langArr[lang].settings,
    onPress: () => navigation.navigate('Settings'),
    Icon: Settings,
  },
  {
    name: langArr[lang].allCategories,
    onPress: () => navigation.navigate('Category'),
    Icon: Category,
  },
];
export default function MainDrawer({ navigation }) {
  const { lang } = useSelector((state: any) => state.lang);

  const renderItems = useMemo(() => {
    return drawerList(lang, navigation).map(
      ({ name, Icon, onPress }, index) => {
        return (
          <Scale activeScale={0.92} onPress={onPress} key={index}>
            <ChoiceBlock>
              <Icon width={50} height={50} />
              <TextFiled style={{ paddingTop: 15 }}>{name}</TextFiled>
            </ChoiceBlock>
          </Scale>
        );
      },
    );
  }, [lang]);

  return (
    <DrawerBlock>
      <ChoiceWrap>{renderItems}</ChoiceWrap>
      <Scale
        style={{ alignItems: 'center' }}
        onPress={() => Linking.openURL('http://indev.uz')}>
        <Indev />
      </Scale>
    </DrawerBlock>
  );
}

const ChoiceWrap = styled.View`
  flex: 1;
  justify-content: center;
`;
const ChoiceBlock = styled.View`
  width: 100%;
  padding: 10px;
  min-height: 122px;
  align-items: center;
  justify-content: center;
  background-color: ${global.colors.secondaryColor};
  border-radius: 12px;
  margin-bottom: 10px;
`;
const DrawerBlock = styled.View`
  height: ${global.strings.height}px;
  padding: 10px;
  background-color: ${global.colors.white};
  z-index: 10;
`;
