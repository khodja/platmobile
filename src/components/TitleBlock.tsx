import React from 'react';
import { DynamicWrap, TextFiled } from '../resources/styled';
import styled from 'styled-components/native';
import global from '../resources/global';
import { Image } from 'react-native';
export default function TitleBlock({ title, image = null, marginTop = null }) {
  return (
    <Wrap style={marginTop && { marginTop }}>
      <DynamicWrap jusify="space-between">
        <TextFiled size="24px" weight="700">
          {title}
        </TextFiled>
        {image && (
          <Image source={{ uri: image }} style={{ width: 30, height: 30 }} />
        )}
      </DynamicWrap>
      <BottomLine />
    </Wrap>
  );
}

const Wrap = styled.View`
  margin-top: 30px;
`;
const BottomLine = styled.View`
  width: 100%;
  height: 1px;
  border-radius: 1px;
  background-color: ${global.colors.mainTextColor};
  margin-top: 15px;
  margin-bottom: 20px;
  opacity: 0.15;
`;
