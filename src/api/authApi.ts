import HttpClient from "../utils/httpClient";
import { HttpConfig } from "./httpConfig";

export const loginUser = (query) => {
  return HttpClient.doPost("/auth", query);
};
