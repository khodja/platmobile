import HttpClient from '../utils/httpClient';

export const searchApi = (query = {}) => {
  return HttpClient.doGet('/search', query);
};
