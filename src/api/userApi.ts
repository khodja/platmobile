import HttpClient from "../utils/httpClient";
import { HttpConfig } from "./httpConfig";

export const getUser = (query) => {
  return HttpClient.doGet("/user", query);
};
