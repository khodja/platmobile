import HttpClient from "../utils/httpClient";

export const sliderApi = (query) => {
  return HttpClient.doGet("/slider", query);
};