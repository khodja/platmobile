import HttpClient from '../utils/httpClient';

export const getCategoryApi = (query) => {
  return HttpClient.doGet('/category', query);
};

export const getCategoryByIdApi = ({ id, lang, page = 1 }) => {
  return HttpClient.doGet('/category/' + id, { lang, page });
};
export const getFoodByAuthorApi = ({ id, lang, page = 1 }) => {
  return HttpClient.doGet('/author/' + id + '/receipt', { lang, page });
};
