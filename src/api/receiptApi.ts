import HttpClient from '../utils/httpClient';

export const getReceiptApi = (query) => {
  return HttpClient.doGet('/receipt', query);
};

export const getReceiptByIdApi = (query) => {
  return HttpClient.doGet('/receipt/' + query.id, query);
};
