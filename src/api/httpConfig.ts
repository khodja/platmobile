export const HttpConfig = {
  API_VERSION: 'v1/',
  STORE_VERSION: '1.0.0',
  API_PATH: 'https://plate.uz/api',
  BASE_URL: 'https://plate.uz/api',
};
